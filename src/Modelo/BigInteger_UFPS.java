/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.math.BigInteger;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS {

    private int[] miNumero;

    public BigInteger_UFPS() {
    }

    /**
     * @param miNumero Una cadena de texto que debe contener un numero entero
     * sin signos ni texto
     */
    public BigInteger_UFPS(String miNumero) {
        int x = miNumero.length();
        String[] s = miNumero.split("");
        this.miNumero = new int[x];
        for (int i = 0; i < x; i++) {
            this.miNumero[i] = Integer.parseInt(s[i]);
        }
    }

    /**
     * @return Cadena de enteros
     */
    public int[] getMiNumero() {
        return miNumero;
    }

    /**
     * Mutiplica dos enteros BigInteger
     *
     * @param dos
     * @return Numero BigInteger resultante de la multiplicacion por "dos"
     */
    public BigInteger_UFPS multiply(BigInteger_UFPS dos) {
        int x[] = dos.getMiNumero();
        String s[] = new String[x.length];
        String Cero = "";
        int lleva = 0;
        for (int i = x.length - 1; i > -1; i--) {
            for (int j = miNumero.length - 1; j > -1; j--) {
                int val = miNumero[j] * x[i] + lleva;
                if (val > 9) {
                    lleva = val / 10;
                } else {
                    lleva = 0;
                }
                String soloultimo = val % 10 + "";
                if (s[i] == null) {
                    s[i] = "";
                }
                s[i] = soloultimo + s[i];
            }
            if (lleva > 0) {
                s[i] = lleva + s[i];
            }
            s[i] = s[i] + Cero;
            Cero += 0;
        }
        return sumatoria(s);
    }

    /**
     * Suma los valores ingresados en una cadena de strings llamando al metodo
     * suma
     *
     * @param valores El parametro valores es una cadena de strings que
     * contienen numeros representados como texto
     * @return El resultado de la suma de multiples numeros BigInteger
     */
    private BigInteger_UFPS sumatoria(String[] valores) {
        BigInteger_UFPS primero = new BigInteger_UFPS(valores[0]);
        for (int i = 1; i < valores.length; i++) {
            BigInteger_UFPS dos = new BigInteger_UFPS(valores[i]);
            primero = primero.correccionPreSuma(dos);
        }
        return primero;
    }

    /**
     * Recibe dos objetos big integer y los convierte a string, compara sus
     * tamaños y los iguala llama al metodo sumaIguales
     *
     * @param dos El parametro dos es un numero BigInteger con el cual se
     * realizara la correccion y posteriormente la suma
     * @return Numero BigInteger resultante de una suma entre otros 2 numeros
     */
    public BigInteger_UFPS correccionPreSuma(BigInteger_UFPS dos) {
        String numDos = "";
        String resultado = "";
        int varCero = dos.getMiNumero().length - this.miNumero.length;
        BigInteger_UFPS nuevo;
        if (varCero > 0) {
            for (int i = 0; i < varCero; i++) {
                numDos += "0";
            }
            numDos += this.toString();
            nuevo = new BigInteger_UFPS(numDos);
            resultado = sumaIguales(nuevo.getMiNumero(), dos.getMiNumero());
        } else if (varCero < 0) {
            varCero *= -1;
            for (int i = 0; i < varCero; i++) {
                numDos += "0";
            }
            numDos += dos.toString();
            nuevo = new BigInteger_UFPS(numDos);
            resultado = sumaIguales(nuevo.getMiNumero(), this.getMiNumero());
        } else {
            resultado = sumaIguales(this.getMiNumero(), dos.getMiNumero());
        }
        BigInteger_UFPS resultadoBI = new BigInteger_UFPS(resultado);
        return resultadoBI;
    }

    /**
     * Suma dos cadenas de numeros del mismo tamaño
     *
     * @param sumandoUno Es una cadena de numeros
     * @param sumandoDos Es una cadena de numeros
     * @return El resultado de la suma entre a y b
     */
    public String sumaIguales(int[] sumandoUno, int[] sumandoDos) {
        int lleva = 0;
        String resultado = "";
        for (int i = sumandoUno.length-1; i > -1; i--) {
            int val = sumandoUno[i] + sumandoDos[i] + lleva;
            if (val > 9) {
                lleva = val / 10;
            } else {
                lleva = 0;
            }
            String ultimo = val % 10 + "";
            resultado = ultimo + resultado;
        }
        if (lleva > 0) {
            resultado = lleva + resultado;
        }
        return resultado;
    }

    /**
     * @return El numero BigInteger en forma de String
     */
    public String toString() {
        String cadena = "";
        for (int x : this.miNumero) {
            cadena += x;
        }
        return cadena;
    }

    /**
     * Retorna la representación entera del BigInteger_UFPS
     *
     * @return un entero
     */
    public int intValue() {
        int valor = Integer.parseInt(this.toString());
        if(this.toString().length() > 9) return Integer.MAX_VALUE;
        return valor;
    }
}
